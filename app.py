from flask import Flask, render_template, request, redirect, url_for
from datetime import datetime

app = Flask(__name__)

@app.route('/')
def index():
    return "Testing SDK"

if __name__ == "__main__":
    app.run(debug=True)